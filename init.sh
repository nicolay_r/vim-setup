# install dependencies
sudo apt-get install vim curl

# copy file
cp settings.vimrc ~/.vimrc

# install pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# install airline
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline

# install jedi
cd ~/.vim/bundle/ && git clone --recursive https://github.com/davidhalter/jedi-vim.git

# install NERD tree
cd ~/.vim/bundle && git clone https://github.com/scrooloose/nerdtree.git

# python PEP8 checker
sudo apt-get install pip
sudo pip install --upgrade pip
sudo pip install flake8
cd ~/.vim/bundle && git clone https://github.com/nvie/vim-flake8.git

# Python Folding Plugin dependencies
mkdir -p ~/.vim/ftplugin
wget -O ~/.vim/ftplugin/python_editing.vim http://www.vim.org/scripts/download_script.php?src_id=5492

# Easy motion plugin
git clone https://github.com/easymotion/vim-easymotion ~/.vim/bundle/vim-easymotion

# Install Silver Search
apt-get install silversearcher-ag
