## Used references

[O'Relly Presentation additional materials](
    https://docs.google.com/file/d/0Bx3f0gFZh5Jqc0MtcUstV3BKdTQ/edit)

[![Damian Conway, "More Instantly Better Vim" - OSCON 2013](
    http://img.youtube.com/vi/aHm36-na4-4/0.jpg)](
    http://www.youtube.com/watch?v=aHm36-na4-4)
