execute pathogen#infect()

set linespace=0
set encoding=utf-8
set t_Co=256
set guifont=Liberation_Mono_for_Powerline:h10
colorscheme desert

" enable 8-bit for ALT button
map <m-a> ggVG
execute "set <M-f>=\ef"
execute "set <M-r>=\er"

" Enable syntax highlighting
    syntax on
    filetype plugin indent on

" enable colored columns after 80'th char
    highlight ColorColumn ctermbg=234
    let &colorcolumn=join(range(81,900),",")
" enable lightning of the current line
    set cursorline
    hi CursorLine cterm=bold ctermbg=234

autocmd BufWritePre * :%s/\s\+$//e

" Rebind <Leader> key
    let mapleader = ","

" show numbers
    set number
    set relativenumber

" Showing relative and norelative numbers
    autocmd InsertEnter * :set norelativenumber
    autocmd InsertLeave * :set relativenumber
    set scrolloff=5
    nnoremap <F2> :set invpaste paste?<CR>
    set pastetoggle=<F2>
    set showmode

" comfortable moving around the long line
"    nnoremap <up> gk
"    nnoremap <down> gj

" Highlight all matches
    set hlsearch
    hi Search ctermbg=228

" Removes highlight of your last search
    noremap <C-n> :nohl<CR>
    vnoremap <C-n> :nohl<CR>
    inoremap <C-n> :nohl<CR>

" Easier moving of code blocks
" Go to 'Visual Mode', then select several lines of code here and
" then press ``>`` several times.
    vnoremap < <gv  " better indentation
    vnoremap > >gv  " better nd nohl

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
    set nobackup
    set nowritebackup
    set noswapfile

" Saving state of folds before leaving and restore saved state after entering
" au BufWinLeave * mkview
" au BufWinEnter * silent loadview

" map sort function to a key
    vnoremap <Leader>s :sort<CR>

" =============================================================================
" Python IDE plugins setups
" =============================================================================

" airline plugin
    let g:airline#extensions#tabline#enabled = 1
    let g:airline#extensions#tabline#fnamemod = ':t'
    let g:airline_left_sep=''
    let g:airline_right_sep=''
    let g:airline_powerline_fonts = 1

" Adding Breakpoints
    map <C-b> Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>

" Jedi-Vim: Python Autocomplete
    let g:jedi#completions_enabled = 1
    let g:jedi#goto_command = "<F12>"
    let g:jedi#rename_command = "<M-r>"
    let g:jedi#usages_command = "<M-f>"
    highlight Pmenu ctermfg=15 ctermbg=234 guifg=#ffffff guibg=#1c1c1c
    autocmd BufWritePost *.py call Flake8()

" Better navigating through omnicomplete option list
" See http://stackoverflow.com/questions/2170023/how-to-map-keys-for-popup-menu-in-vim
    set completeopt=longest,menuone
    function! OmniPopup(action)
        if pumvisible()
            if a:action == 'j'
                return "\<C-N>"
            elseif a:action == 'k'
                return "\<C-P>"
            endif
        endif
        return a:action
    endfunction

    inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
    inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" offset settings (Python style settings)
" Real programmers don't use TABs but spaces
    set tabstop=4
    set shiftwidth=4
    set softtabstop=4
    set expandtab
    set shiftround
    set backspace=2
    set laststatus=2

" Python folding
" mkdir -p ~/.vim/ftplugin
" wget -O ~/.vim/ftplugin/python_editing.vim http://www.vim.org/scripts/download_script.php?src_id=5492
    set nofoldenable

" =============================================================================
" Latex IDE features setups
" =============================================================================

" Easier formatting of paragraphs (Wraps words to the length of 80-th chars)
    vmap Q gq
    nmap Q gqap

" Easy motion plugin
    map <Leader> <Plug>(easymotion-prefix)

" =============================================================================
" Silver Searcher
" =============================================================================

if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>
